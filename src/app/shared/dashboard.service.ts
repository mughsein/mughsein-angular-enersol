import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class DashboardService {
  endpoint: string = 'http://test-demo.aemenersol.com/api';
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  dashboard = {};

  constructor(
    private http: HttpClient,
    public router: Router
  ) {
  }

  getDashboard() {
    let api = `${this.endpoint}/dashboard`;
    return this.http
    .get(api, { headers: this.headers })
      .toPromise();
  }

  // Error 
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
}