import { Injectable } from '@angular/core';
import { User } from './user';
import { from, Observable, throwError } from 'rxjs';
import { catchError, tap, switchMap, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  endpoint: string = 'http://test-demo.aemenersol.com/api';
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  currentUser = {};

  constructor(
    private http: HttpClient,
    public router: Router
  ) {
  }

  // Sign-up
  signUp(user: User): Observable<any> {
    let api = `${this.endpoint}/register-user`;
    return this.http.post(api, user)
      .pipe(
        catchError(this.handleError)
      )
  }

  // Sign-in
  signIn(user: User) {
    return this.http.post<any>(`${this.endpoint}/account/login`, user)
      .subscribe((res: any) => {
        localStorage.setItem('access_token', res)
        this.router.navigate(['dashboard']);
      })
  }
  
  // Sign in a user and store access and refresh token
  login(user: User): Observable<any> {
    return this.http.post(`${this.endpoint}/account/login`, user).pipe(
      switchMap((res: any) => {
        const storeAccess = localStorage.setItem('access_token', res)
        return from(Promise.all([storeAccess]));
      }),
      catchError(this.handleError)
    )
  }

  getToken() {
    let token =  localStorage.getItem('access_token');
    console.log(token)
    return token;
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');
    return (authToken !== null) ? true : false;
  }

  doLogout() {
    let removeToken = localStorage.removeItem('access_token');
    if (removeToken == null) {
      this.router.navigate(['log-in']);
    }
  }

  // User profile
  getUserProfile(id): Observable<any> {
    let api = `${this.endpoint}/user-profile/${id}`;
    return this.http.get(api, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.handleError)
    )
  }

  // Error 
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
}