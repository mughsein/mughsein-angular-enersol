import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { AuthService } from "./../../shared/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.css"],
})
export class SigninComponent implements OnInit {
  signinForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router
  ) {
    this.signinForm = this.fb.group({
      username: [""],
      password: [""],
    });
  }

  ngOnInit() {}

  loaderVisibility(visibility:string){
    document.getElementById("loading-container").style.visibility = visibility;
  }

  loginUser() {
    this.loaderVisibility("visible");
    this.authService.login(this.signinForm.value).subscribe(
      async (_) => {
        this.loaderVisibility("hidden");
        this.router.navigate(['dashboard']);
      },
      async (res) => {
        this.loaderVisibility("hidden");
        let buttons = null;
      }
    );
  }
}
