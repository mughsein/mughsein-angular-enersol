import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import * as d3 from "d3";
import { DashboardService } from "src/app/shared/dashboard.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent implements OnInit {
  dashboardObj: any = {};
  chartDonut: any;
  chartBar: any;
  tableUsers: any;
  private svgdonut: any;
  private svgbar: any;
  private margin = 20;
  private realWidth = 400;
  private realHeight = 400;
  private width = this.realWidth - this.margin * 2;
  private height = this.realHeight - this.margin * 2;
  private barMax = 100;
  private radius = Math.min(this.realWidth, this.realHeight) / 2;
  private donutWidth = 75;

  constructor(
    public dashboardService: DashboardService,
    private actRoute: ActivatedRoute
  ) {
    this.dashboardService.getDashboard().then((result: any) => {
      this.dashboardObj = result;
      this.chartDonut = result.chartDonut;
      this.chartBar = result.chartBar;
      this.tableUsers = result.tableUsers;
      this.barMax = Math.max.apply(
        Math,
        this.chartBar.map(function (o) {
          return o.value;
        })
      );
      console.log(this.dashboardObj);
      this.initDonut();
      this.drawBars(this.chartBar);
    });
  }

  ngOnInit(): void {
    this.initBar();
  }

  private initDonut(): void {
    let dataset: any = this.chartDonut;
    let width = 360;
    let height = 360;
    let radius = Math.min(width, height) / 2;
    let donutWidth = 75; // NEW

    let color : any= d3
      .scaleOrdinal()
      .range([
        "#b4b4b4",
        "#979797",
        "#7a7a7a",
        "#5f5f5f",
        "#404040",
        "#1e1e1e",
        "#000000",
      ]);

    let svg = d3
      .select("div#donutcontainer")
      .append("svg")
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", "0 0 " + this.width + " " + this.realHeight)
      .append("g")
      .attr("transform", "translate(" + 360 / 2 + "," + 360 / 2 + ")")
      .classed("svg-content", true);

    let arc: any = d3
      .arc()
      .innerRadius(radius - donutWidth) // NEW
      .outerRadius(radius);

    let pie = d3
      .pie()
      .value(function (d: any) {
        return d.value;
      })
      .sort(null);

    let path: any = svg
      .selectAll("path")
      .data(pie(dataset))
      .enter()
      .append("path")
      .attr("d", arc)
      .attr("fill", function (d: any, i) {
        return color(d.data.name);
      });
  }

  private initBar(): void {
    this.svgbar = d3
      .select("div#barcontainer")
      .append("svg")
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", "0 0 " + this.width + " " + this.realHeight)
      .append("g")
      .attr("transform", "translate(" + this.margin + ", 10)")
      .classed("svg-content", true);
  }

  private drawBars(data: any[]): void {
    // Create the X-axis band scale
    const x = d3
      .scaleBand()
      .range([0, this.width])
      .domain(data.map((d) => d.name))
      .padding(0.2);

    // Draw the X-axis on the DOM
    this.svgbar
      .append("g")
      // .attr("transform", "translate(0," + this.height + ")")
      .attr("transform", "translate(0," + Number(this.realHeight - 40) + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    // Create the Y-axis band scale
    const y = d3.scaleLinear().domain([0, this.barMax]).range([this.height, 0]);

    // Draw the Y-axis on the DOM
    this.svgbar.append("g").call(d3.axisLeft(y));

    // Create and fill the bars
    this.svgbar
      .selectAll("bars")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", (d) => x(d.name))
      .attr("y", (d) => y(d.value))
      .attr("width", x.bandwidth())
      .attr("height", (d) => this.height - y(d.value))
      .attr("fill", "#9E9E9E");
  }
}
